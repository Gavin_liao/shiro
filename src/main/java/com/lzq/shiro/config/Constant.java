package com.lzq.shiro.config;

public class Constant {
	
	public static final String RSP_STATUS = "status";
	public static final String SUCCESS_STATUS = "ok";
	public static final String FAILED_STATUS = "failed";
	
	
	public static final String CORE_SERVICE_ERR_CODE = "00000";
	public static final String CORE_SERVICE_ERR_MESSAE = "内部服务异常";
	
	public static final String MOBILE_ALREADY_EXIST_CODE = "00001";
	public static final String MOBILE_ALREADY_EXIST_MESSAGE = "手机号码已被注册";
	
	public static final String REGISTER_FAILED_CODE = "00002";
	public static final String REGISTER_FAILED_MESSAGE = "用户注册失败";
	
	public static final String PERMISSION_DENIED_CODE = "00003";
	public static final String PERMISSION_DENIED_MESSAGE = "权限不足";
	
	public static final String NOT_LOGIN_CODE = "00004";
	public static final String NOT_LOGIN_MESSAGE = "用户不处于登录状态";
	
	public static final String NOT_FOUND_CODE = "00005";
	public static final String NOT_FOUND_MESSAGE = "访问的页面不存在";
	
	// 系统默认用户角色
	public static final String DEFAULT_USER_ROLE = "custom";
	
	public static final String SHIRO_COOKIE_NAME = "shiro_redis_session";
	
	public static final String URL_NOT_AUTHORIZATION = "/403";
	
	public static final String URL_NOT_FOUND = "/404";
	
	
	
	
	
	
}	
