package com.lzq.shiro.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import com.lzq.shiro.entity.ResponseEntity;

/**
 * Created by Administrator on 2017/12/11.
 * 全局异常处理
 */

@Order(-1000)
public class MyExceptionHandler implements HandlerExceptionResolver {


    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception ex) {
        ModelAndView mv = new ModelAndView();
        FastJsonJsonView view = new FastJsonJsonView();
        Map<String, Object> attributes = new HashMap<String, Object>();
        if (ex instanceof UnauthenticatedException) {
            attributes.put("code", "1000001");
            attributes.put("msg", "token错误");
        } else if (ex instanceof UnauthorizedException) {
            attributes.put("code", "1000002");
            attributes.put("msg", "用户无权限");
        } if (ex instanceof org.springframework.web.servlet.NoHandlerFoundException) {
        	NoHandlerFoundException ne = (NoHandlerFoundException)ex;
        	String url = ne.getRequestURL();
        	if(Constant.URL_NOT_AUTHORIZATION.equals(url)) {
        		attributes.put("code", ResponseEntity.PERMISSION_DENIED.getCode());
        		attributes.put("msg", ResponseEntity.PERMISSION_DENIED.getMessage());
        	}
        }else {
            attributes.put("code", "1000003");
            attributes.put("msg", ex.getMessage());
            ex.printStackTrace();
        }

        view.setAttributesMap(attributes);
        mv.setView(view);
        return mv;
    }
}
