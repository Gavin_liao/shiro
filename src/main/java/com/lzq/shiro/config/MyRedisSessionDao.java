package com.lzq.shiro.config;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

@SuppressWarnings({"rawtypes","unchecked"})
public class MyRedisSessionDao extends EnterpriseCacheSessionDAO {
	public static Logger logger = LoggerFactory.getLogger(MyRedisSessionDao.class);
	
	
	// private RedisTemplate<String,byte[]> redisTemplate;
	private RedisTemplate<String,Object> redisTemplate;
    public MyRedisSessionDao(RedisTemplate redisTemplate){
    	if(null==redisTemplate) {
    		System.out.println("初始化为空");
    	}
    	this.redisTemplate = redisTemplate;

    }
	
	public MyRedisSessionDao() {
		System.out.println("开始打印MyRedisSessioDao中的构造信息");
		System.out.println(this.redisTemplate);
		if(this.redisTemplate != null) {
			System.out.println(this.redisTemplate.getKeySerializer());
			System.out.println(this.redisTemplate.getValueSerializer());
		}
	}

	@Override
    protected Serializable doCreate(Session session) {

		Serializable sessionId = super.doCreate(session);
        // redisTemplate.opsForValue().set(sessionId.toString(),sessionToByte(session));
		redisTemplate.opsForValue().set(sessionId.toString(),session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
    	System.out.println("读取session");
        Session session = super.doReadSession(sessionId);
        if(session == null){
         /* byte[] sessionByte = redisTemplate.opsForValue().get(sessionId.toString());
          if(null!=sessionByte) {
        	  session =  byteToSession(sessionByte);
          }*/
          session = (SimpleSession) redisTemplate.opsForValue().get(sessionId.toString());
        }
        return session;
    }

    //设置session的最后一次访问时间
	@Override
    protected void doUpdate(Session session) {
        super.doUpdate(session);
        logger.debug("更新seesion,id=[{}]", session.getId().toString());
		try {
			// redisTemplate.opsForValue().set(session.getId().toString(), sessionToByte(session));
			redisTemplate.opsForValue().set(session.getId().toString(), session);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}

    }

    // 删除session
	@Override
    protected void doDelete(Session session) {
        super.doDelete(session);
        redisTemplate.delete(session.getId().toString());
    }

   /*private byte[] sessionToByte(Session session){
        if (null == session){
            return null;
        }
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        byte[] bytes = null;
        ObjectOutputStream oo ;
        try {
            oo = new ObjectOutputStream(bo);
            oo.writeObject(session);
            bytes = bo.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return bytes;

    }
    private Session byteToSession(byte[] bytes){
        if(0==bytes.length){
            return null;
        }
        ByteArrayInputStream bi = new ByteArrayInputStream(bytes);
        ObjectInputStream in;
        SimpleSession session = null;
        try {
            in = new ObjectInputStream(bi);
            session = (SimpleSession) in.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        return session;
    }*/
}
