package com.lzq.shiro.config.redis;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Value("${redis.shiro.host}")
	private String host;
	
	@Value("${redis.shiro.port}")
	private int port;
	
	@Value("${redis.shiro.timeout}")
	private int timeout;
	
	@Value("${redis.shiro.maxIdle}")
	private int maxIdle;
	
	@Value("${redis.shiro.maxWaitMillis}")
	private long maxWaitMillis;
	
	@Value("${redis.shiro.password}")
	private String password;
	
	@Value("${redis.shiro.database}")
	private int database;
	
	
	/*@Bean
	public CacheManager cacheManager(RedisTemplate redisTemplate) {
		return new RedisCacheManager(redisTemplate);
	}*/
	
	@Bean
	@ConfigurationProperties(prefix="redis.shiro")
	public JedisPoolConfig jedisPoolConfig(){
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(maxIdle);
		config.setMaxWaitMillis(maxWaitMillis);
		return config;
	}
	
	@Bean
	public JedisConnectionFactory redisConnectionFactory() {
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
		JedisPoolConfig poolConfig = jedisPoolConfig();
		jedisConnectionFactory.setPoolConfig(poolConfig);
		jedisConnectionFactory.setHostName(host);
		jedisConnectionFactory.setPort(port);
		jedisConnectionFactory.setTimeout(timeout);
		jedisConnectionFactory.setPassword(password);
		jedisConnectionFactory.setDatabase(database);
		return jedisConnectionFactory;
	}
	
	
	@Bean("redisTemplate")
	public RedisTemplate<Object,Object> redisTemplate(RedisConnectionFactory redisCF){
		RedisTemplate<Object,Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(redisCF);
		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
//		redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//		redisTemplate.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
//		redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
		redisTemplate.afterPropertiesSet();
		return redisTemplate;
	}
	
	@Bean(name="stringRedisTemplate")
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisCF) {
		StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
		
		stringRedisTemplate.setConnectionFactory(redisCF);
		stringRedisTemplate.setKeySerializer(new StringRedisSerializer());
		stringRedisTemplate.setValueSerializer(new StringRedisSerializer());
		return stringRedisTemplate;
	}
	
	
	@Bean(name="defaultRedisTemplate")
	public StringRedisTemplate defaultTemplate(RedisConnectionFactory redisCF) {
		StringRedisTemplate defaultTemplate = new StringRedisTemplate();
		
		defaultTemplate.setConnectionFactory(redisCF);
		defaultTemplate.setKeySerializer(new StringRedisSerializer());
		defaultTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		return defaultTemplate;
	}
	
}
