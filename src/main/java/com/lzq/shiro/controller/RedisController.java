package com.lzq.shiro.controller;



import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lzq.shiro.entity.SysUser;
import com.lzq.shiro.service.RedisService;
import com.lzq.shiro.util.JsonUtil;



@Controller
@RequestMapping("/redis")
public class RedisController {
	private Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private RedisService redisService;
	
	@Resource(name="redisTemplate")
	private RedisTemplate<String , byte[]> redisTemplate;
	
	@PostMapping("/test")
	@ResponseBody
	public String testRedis() {
		redisService.set("test", "123");
		return redisService.get("test");
	}
	
	@PostMapping("/saveSysUser")
	@ResponseBody
	public SysUser saveSysUser() {
		SysUser user = new SysUser();
		user.setId("sys_user_1");
		user.setUsername("liaozongqiang");
		user.setPassword("123456");
		redisService.set(user.getId(), JsonUtil.objToJson(user));
		String userJson = redisService.get(user.getId());
		logger.info(userJson);
		return JsonUtil.jsonToBean(userJson, SysUser.class);
		
	}
	@GetMapping("/session")
	@ResponseBody
	public String session() {
		
		String session = redisService.get("shiro_redis_session:aa513b4b-9a88-42b0-b842-b11b3fce6ee9");
		logger.info(session);
		return session;
	}
	
	
	private Session byteToSession(byte[] bytes){
        if(0==bytes.length){
            return null;
        }
        ByteArrayInputStream bi = new ByteArrayInputStream(bytes);
        ObjectInputStream in;
        SimpleSession session = null;
        try {
            in = new ObjectInputStream(bi);
            session = (SimpleSession) in.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        return session;
    }
	
	@GetMapping("/shiroSession")
	@ResponseBody
	public Object shiroSession() {
		try {
			 byte[] sessionByte = redisTemplate.opsForValue().get("d2e98512-5799-4e2e-9264-49535c24f57e");
			 Session session = (SimpleSession) this.byteToSession(sessionByte);
//			 Object principals = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
//			 logger.info(principals);
//			 Object auths = session.getAttribute(DefaultSubjectContext.AUTHENTICATED_SESSION_KEY);
//			
//			 logger.info(auths);
//			 Map<String , Object> result = new HashMap<String , Object>();
//			 result.put("principals", principals);
//			 result.put("auths", auths);
			 
			 //Object primaryPrincipal = SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
			 
			 //SecurityUtils.getSubject().checkRole("custom");
			 //SecurityUtils.getSubject().checkRole("admin");
			 
			 
			 
			 
			 return session;
		} catch(AuthorizationException ae) {
			ae.printStackTrace();
			return "你没有相关权限";
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
}
