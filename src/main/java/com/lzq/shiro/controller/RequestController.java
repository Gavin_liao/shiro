package com.lzq.shiro.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Administrator
 * @des 请求相关测试
 */
@Controller
@RequestMapping("/req")
public class RequestController {
	
	
	@GetMapping("/cookie")
	@ResponseBody
	public String requestCookie(HttpServletRequest request) {
		Cookie[]  cookies = request.getCookies();
		for(Cookie cookie : cookies) {
			String cookieName = cookie.getName();
			String cookieValue =  cookie.getValue();
			System.out.println(cookieName + ":" + cookieValue);
			System.out.println("---------------------");
		}
		
		return "成功";
	}
	
}
