package com.lzq.shiro.entity;

import org.apache.commons.lang3.StringUtils;

import com.lzq.shiro.config.Constant;

/**
 * @author Administrator
 * @title 请求相应枚举
 */
public enum ResponseEntity {
	
	
	CORE_EXCP(Constant.CORE_SERVICE_ERR_CODE , Constant.CORE_SERVICE_ERR_MESSAE),
	MOBILE_ALR_EXIST(Constant.MOBILE_ALREADY_EXIST_CODE , Constant.MOBILE_ALREADY_EXIST_MESSAGE),
	REGISTER_FAILED(Constant.REGISTER_FAILED_CODE , Constant.REGISTER_FAILED_MESSAGE),
	PERMISSION_DENIED(Constant.PERMISSION_DENIED_CODE,Constant.PERMISSION_DENIED_MESSAGE),
	NOT_LOGIN(Constant.NOT_LOGIN_CODE , Constant.NOT_LOGIN_MESSAGE),
	PAGE_NOT_FOUND(Constant.NOT_FOUND_CODE , Constant.NOT_FOUND_MESSAGE);
	
	private String code;
	
	private String message;
	
	
	ResponseEntity(String code , String message) {
		this.code = code;
		this.message = message;
	}
	
	
	/** 
	 * @title 通过code获取message
	 * @param code
	 * @return
	 */
	public static String getMessage(String code) {
		if(!StringUtils.isBlank(code)) {
			ResponseEntity[] entityArr = ResponseEntity.values();
			for(ResponseEntity entity : entityArr) {
				if(code.equals(entity.getCode())) {
					return entity.getMessage();
				}
			}			
		}
		return null;
	}
	
	
	/**
	 * @title 通过message获取code
	 * @param message
	 * @return
	 */
	public static String getCode(String message) {
		if(!StringUtils.isBlank(message)) {
			ResponseEntity[] entityArr = ResponseEntity.values();
			for(ResponseEntity entity : entityArr) {
				if(message.equals(entity.getMessage())) {
					return entity.getCode();
				}
			}
		}
		return null;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
}
