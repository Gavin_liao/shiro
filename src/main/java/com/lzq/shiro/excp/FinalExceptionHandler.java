package com.lzq.shiro.excp;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lzq.shiro.entity.ResponseEntity;
import com.lzq.shiro.vo.RspUtil;
/*@RestController*/
public class FinalExceptionHandler implements ErrorController{

	
	@RequestMapping(value = "/error")
    public Map<String , Object> error(HttpServletResponse resp, HttpServletRequest req) {
        // 错误处理逻辑
		Map<String , Object> resultMap = RspUtil.rspErrMap(ResponseEntity.CORE_EXCP.getCode(), ResponseEntity.CORE_EXCP.getMessage());
		return resultMap;
    }
	
	@RequestMapping(value = "/403")
    public Map<String , Object> permissionDenied(HttpServletResponse resp, HttpServletRequest req) {
		Map<String , Object> resultMap =  RspUtil.rspErrMap(ResponseEntity.PERMISSION_DENIED.getCode(), ResponseEntity.PERMISSION_DENIED.getMessage());
		return resultMap;
	}
	

	
	@Override
	public String getErrorPath() {
		return "/error";
	}

}
