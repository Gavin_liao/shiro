package com.lzq.shiro.excp;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.lzq.shiro.config.Constant;
import com.lzq.shiro.entity.ResponseEntity;
import com.lzq.shiro.vo.RspUtil;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
	
	private Logger logger = Logger.getLogger(this.getClass());
    
    @ExceptionHandler(NoHandlerFoundException.class)
    public Map<String , Object> notFoundException(NoHandlerFoundException e){
    	
    	String requestUrl = e.getRequestURL();
    	// 用户权限不足
    	if(Constant.URL_NOT_AUTHORIZATION.equals(requestUrl)) {
    		logger.error("用户权限不足");
        	logger.error(e.getMessage() , e);
    		return RspUtil.rspErrMap(ResponseEntity.PERMISSION_DENIED.getCode(), ResponseEntity.PERMISSION_DENIED.getMessage());
    	}
    	logger.error("访问页面不存在");
    	logger.error(e.getMessage(), e);
    	return RspUtil.rspErrMap(ResponseEntity.PAGE_NOT_FOUND.getCode(), ResponseEntity.PAGE_NOT_FOUND.getMessage());
    	
    }
    
    
    @ExceptionHandler(UnauthorizedException.class)
    public Map<String,Object> exceptionHandler(UnauthorizedException e){
    	logger.error("用户权限不足");
    	logger.error(e.getMessage() , e);
        return RspUtil.rspErrMap(ResponseEntity.PERMISSION_DENIED.getCode(), ResponseEntity.PERMISSION_DENIED.getMessage());
    }
    
    @ExceptionHandler(Exception.class)
    public Map<String , Object> exception(Exception e){
    	logger.error("系统未捕获异常");
    	logger.error(e.getMessage() , e);
    	return RspUtil.rspErrMap(ResponseEntity.CORE_EXCP.getCode(), ResponseEntity.CORE_EXCP.getMessage());
    }
    

	
}
