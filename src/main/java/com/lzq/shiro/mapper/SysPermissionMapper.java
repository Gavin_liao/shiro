package com.lzq.shiro.mapper;


import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lzq.shiro.entity.SysPermission;
import com.lzq.shiro.entity.UserInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermission> selectPermByUser(UserInfo userInfo);

}
