package com.lzq.shiro.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lzq.shiro.entity.SysRole;
import com.lzq.shiro.entity.UserInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> selectRoleByUser(UserInfo userInfo);
    
    List<SysRole> selectRole(SysRole sysRole);
}
