package com.lzq.shiro.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lzq.shiro.entity.SysUser;

/**
 * Created by Administrator on 2018/1/10.
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
