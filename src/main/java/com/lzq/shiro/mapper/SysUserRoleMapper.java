package com.lzq.shiro.mapper;

import org.apache.ibatis.annotations.Param;

import com.lzq.shiro.entity.UserRole;

public interface SysUserRoleMapper {

	Integer update(@Param("userRole") UserRole userRole);

	Integer insert(@Param("userRole") UserRole userRole);
	
}
