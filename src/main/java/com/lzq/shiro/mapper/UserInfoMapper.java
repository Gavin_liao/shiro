package com.lzq.shiro.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.lzq.shiro.entity.UserInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
