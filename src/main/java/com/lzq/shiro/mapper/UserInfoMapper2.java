package com.lzq.shiro.mapper;

import org.apache.ibatis.annotations.Param;

import com.lzq.shiro.entity.UserInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
public interface UserInfoMapper2  {

	UserInfo findByMobile(String mobile);

	int addUser(UserInfo userInfo);

	UserInfo queryByParams(UserInfo queryParams);
	
}
