package com.lzq.shiro.service;

/**
 * @author Administrator
 * @desc redis操作类封装
 */
public interface RedisService {
	
	boolean set(String key , String value);
	
	String get(String key);
}
