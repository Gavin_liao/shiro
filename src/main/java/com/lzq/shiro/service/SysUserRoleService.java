package com.lzq.shiro.service;

import java.util.Map;

import com.lzq.shiro.entity.UserRole;

public interface SysUserRoleService {

	Map<String, Object> modify(UserRole userRole) throws Exception;

}
