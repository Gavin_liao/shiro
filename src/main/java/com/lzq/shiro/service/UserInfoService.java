package com.lzq.shiro.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.lzq.shiro.entity.UserInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
public interface UserInfoService extends IService<UserInfo> {



}
