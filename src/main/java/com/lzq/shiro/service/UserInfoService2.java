package com.lzq.shiro.service;

import java.util.Map;

import com.lzq.shiro.entity.UserInfo;

public interface UserInfoService2 {

	Map<String, Object> register(UserInfo userInfo) throws Exception;

	UserInfo query(UserInfo queryParams) throws Exception;
	
}
