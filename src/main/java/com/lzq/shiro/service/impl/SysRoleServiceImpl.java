package com.lzq.shiro.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lzq.shiro.entity.SysRole;
import com.lzq.shiro.entity.UserInfo;
import com.lzq.shiro.mapper.SysRoleMapper;
import com.lzq.shiro.service.SysRoleService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    public List<SysRole> selectRoleByUser(UserInfo userInfo) throws Exception{
        return baseMapper.selectRoleByUser(userInfo);
    }
}
