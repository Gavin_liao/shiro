package com.lzq.shiro.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lzq.shiro.entity.UserRole;
import com.lzq.shiro.mapper.SysUserRoleMapper;
import com.lzq.shiro.service.SysUserRoleService;
import com.lzq.shiro.vo.RspUtil;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService{
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	
	@Override
	public Map<String, Object> modify(UserRole userRole) throws Exception {
		Integer updateFlag = sysUserRoleMapper.update(userRole);
		logger.info(updateFlag);
		return RspUtil.rspMap("成功");
	}

	
}
