package com.lzq.shiro.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lzq.shiro.entity.UserInfo;
import com.lzq.shiro.mapper.UserInfoMapper;
import com.lzq.shiro.service.UserInfoService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huitu123
 * @since 2018-01-23
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {



}
