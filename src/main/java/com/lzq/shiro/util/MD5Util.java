package com.lzq.shiro.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author lzq
 * @desc 对md5加密算法进行加盐处理，防止DBA用户从数据库中获取用户密码进行破解
 */
public class MD5Util {

	// 生成salt
	public static byte[] createSalt() {
		byte[] salt = new byte[16];
		try {
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			random.nextBytes(salt);
			
			System.out.println(byteToHexString(salt));
			return salt;
		} catch (NoSuchAlgorithmException e) {

		}
		return null;
	}

	// 生成加密后的密码
	public static byte[] digest(String password, byte[] salt) {
		MessageDigest msgDigest;
		try {
			msgDigest = MessageDigest.getInstance("MD5");
			if (salt != null && salt.length > 0) {
				msgDigest.update(salt);
			}
			byte[] digest = msgDigest.digest(password.getBytes());

			return digest;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String createCredential(String password) throws UnsupportedEncodingException {
		return byteToHexString(digest(password, createSalt()));
	}

	public static String createCredential(String password, String salt) throws UnsupportedEncodingException {
		return byteToHexString(digest(password, salt.getBytes()));
	}

	private static final String HEX_NUMS_STR = "0123456789ABCDEF"; // 16进制字符串

	/**
	 * 将16进制字符串转换成字节数组
	 * 
	 * @param hex
	 * @return
	 */
	public static byte[] hexStringToByte(String hex) {
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		// 将16进制数转化为字符数组
		char[] hexChars = hex.toCharArray();

		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (HEX_NUMS_STR.indexOf(hexChars[pos]) << 4 | HEX_NUMS_STR.indexOf(hexChars[pos + 1])); // 此处不懂？？？？
		}
		return result;
	}

	/**
	 * 将指定byte数组转换成16进制字符串
	 * 
	 * @param b
	 * @return
	 */
	public static String byteToHexString(byte[] b) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			// 将Integer类型的参数转换成16进制的字符串
			String hex = Integer.toHexString(b[i] & 0xFF);
			// 如果16进制字符串的长度为1，在字符串前面添加一个0
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			hexString.append(hex.toUpperCase());
		}
		return hexString.toString();
	}

}
