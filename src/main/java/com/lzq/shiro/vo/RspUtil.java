package com.lzq.shiro.vo;

import java.util.HashMap;
import java.util.Map;

public class RspUtil {
	
	public static Map<String , Object> rspErrMap(String  errCode , Object msg){
		Map<String , Object> result = new HashMap<String , Object>();
		result.put("status", "failed");
		result.put("errCode", errCode);
		result.put("msg", msg);
		return result;
	}
	
	public static Map<String , Object> rspMap(Object obj){
		Map<String , Object> result = new HashMap<String , Object>();
		result.put("status", "ok");
		result.put("data", obj);
		return result;
	}
	
}
